from random import randint
import time
from selenium import webdriver  
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

driver = webdriver.Chrome("D:\Documents\Professionel\WebBot\chromedriver.exe")

#Return a random time in second as int
def getRandomExecTime(min=.4,max=2.3):
    random_int= randint(0, 800)
    time=(max-min)*random_int/800+min
    return time

#send an omegle message
def send_message(text):
    if check_nonfinished_chat:
        time.sleep(getRandomExecTime())
        area_text_msg=driver.find_element_by_class_name("chatmsg ")
        area_text_msg.send_keys(text)

        time.sleep(getRandomExecTime())

        button_send_msg=driver.find_element_by_class_name("sendbtn")
        button_send_msg.click()
    else:
        restart_chat()

#start a chat from the initial omegle page
def start_chat_from_home():
    time.sleep(getRandomExecTime())

    button_start_chat=driver.find_element_by_id("chattypetextcell")
    button_start_chat.click()

    time.sleep(getRandomExecTime())

    labels_check_acceptation=driver.find_elements_by_css_selector("p")
    for label in labels_check_acceptation:
        if '<input type="checkbox"' in label.get_attribute("outerHTML"):
            time.sleep(getRandomExecTime())
            label.click()

    time.sleep(getRandomExecTime())
    button_confirm_acceptation=driver.find_element_by_css_selector("input[type=button]")
    button_confirm_acceptation.click()

#Check if the other user quit the chat
def check_nonfinished_chat():
    button_disconnet=driver.find_element_by_class_name("disconnectbtn")
    if "New" in button_disconnet.get_attribute("outerHTML"):
        return False
    else:
        return True

#Restart a new chat
def restart_chat():
    button_disconnet=driver.find_element_by_class_name("disconnectbtn")
    if "New" in button_disconnet.get_attribute("outerHTML"):
        button_disconnet.click()
        return True
    elif "Stop" in button_disconnet.get_attribute("outerHTML"):
        button_disconnet.click()
        time.sleep(.1)
        button_disconnet.click()
        return restart_chat()
    elif "Really" in button_disconnet.get_attribute("outerHTML"):
        button_disconnet.click()
        time.sleep(.1)
        return restart_chat()
    else:
        return True


driver.get("https://www.omegle.com/")
start_chat_from_home()
send_message("Salut toi!")



time.sleep(10)
driver.close()